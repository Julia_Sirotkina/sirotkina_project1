//
//  InterfaceController.swift
//  Pogoda WatchKit Extension
//
//  Created by WSR on 6/22/19.
//  Copyright © 2019 WSR. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON




class InterfaceController: WKInterfaceController {
    @IBOutlet weak var ImgPogoda: WKInterfaceImage!
    @IBOutlet weak var temper: WKInterfaceLabel!
    @IBOutlet weak var LabelCity: WKInterfaceLabel!
    
    var city = "Moscow"
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        downloadData(city: city)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    func downloadData(city: String) {
        //прописываем токен (ключ доступа к данным которые храняться где то ...)
        let token = "1e936ee21707e2a418e98dca00877357"
        //прописываем ссылку ()
        let urlStr = "https://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&appid=\(token)"
        //  let url = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        //отправляем запрос к на сервер
        Alamofire.request(urlStr, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                //записываем значение в наш город
                self.LabelCity.setText(city)
                // в label temper записываем температуру (запрос с ервера)
                self.temper.setText(json["main"]["temp"].stringValue+"°C")
                let mintemp = json["main"]["temp_min"].stringValue+"°C"
                let maxtemp = json["main"]["temp_max"].stringValue+"°C"
                UserDefaults.standard.set(maxtemp, forKey: "maxtemp")
                UserDefaults.standard.set(mintemp, forKey: "mintemp")
                //= json["main"]["temp"].stringValue+"°C"
               
                //работа с иконками
                let  icon = json["weather"][0]["icon"].stringValue
                let imageStr = "https://api.openweathermap.org/img/w/" + (icon) + ".png"
                let imageUrl = URL(string: imageStr)
                let data = try? Data(contentsOf: imageUrl!)
                self.ImgPogoda.setImage(UIImage(data: data!))
                
            //если запрос не срабатывает то выдаем ошибку
            case .failure(let error):
                print(error)
            }
        }
        
    }

}
