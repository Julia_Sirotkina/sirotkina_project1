//
//  newInterfaceController.swift
//  
//
//  Created by WSR on 6/22/19.
//

import WatchKit
import Foundation


class newInterfaceController: WKInterfaceController {

    @IBOutlet weak var maxTemper: WKInterfaceLabel!
    @IBOutlet weak var minTemper: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        //сщздаем переменные maxtemp and mintemp / записываем в них значения которые беруться с сервера
        //UserDefaults это локальный кэш
        maxTemper.setText(UserDefaults.standard.string(forKey: "maxtemp"))
        minTemper.setText(UserDefaults.standard.string(forKey: "mintemp"))
        super.willActivate()
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
